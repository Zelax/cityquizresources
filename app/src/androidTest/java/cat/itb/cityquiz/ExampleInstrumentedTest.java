package cat.itb.cityquiz;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;



import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


import cat.itb.cityquiz.Presentation.WelcomeActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;
/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        assertEquals("cat.itb.cityquiz", appContext.getPackageName());
    }
    @Rule
    public ActivityTestRule<WelcomeActivity> mActivityTestRule = new ActivityTestRule<>(WelcomeActivity.class);


    @Test
    public void iWantSomethingToWorkTest() {
        // test code here

    }
    @Test
    public void clickRollDisplayResulTest(){

        onView(withId(R.id.StartButton))
                .perform(click());
        onView(withId(R.id.button20))
                .perform(click());
        onView(withId(R.id.button21))
                .perform(click());
        onView(withId(R.id.button22))
                .perform(click());
        onView(withId(R.id.button23))
                .perform(click());
        onView(withId(R.id.button24))
                .perform(click());
        onView(withId(R.id.button25))
                .perform(click());
        onView(withId(R.id.RestarGAme))
                .perform(click());
    }
}
