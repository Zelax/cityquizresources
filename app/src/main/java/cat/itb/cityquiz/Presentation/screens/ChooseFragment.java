package cat.itb.cityquiz.Presentation.screens;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.Answer;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;

public class ChooseFragment extends Fragment {

    @BindView(R.id.button20)
    Button button20;
    @BindView(R.id.button21)
    Button button21;
    @BindView(R.id.button22)
    Button button22;
    @BindView(R.id.button23)
    Button button23;
    @BindView(R.id.button24)
    Button button24;
    @BindView(R.id.button25)
    Button button25;
    @BindView(R.id.Swaperphoto)
    ImageView Swaperphoto;
    private ChooseViewModel mViewModel;





    public static ChooseFragment newInstance() {
        return new ChooseFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.choose_fragment, container, false);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(ChooseViewModel.class);

        Game game = mViewModel.getGame();
        Question question = game.getCurrentQuestion();

        if (mViewModel.getGame().isFinished()){
            goEndFragment();
        }else {
            displayGame(question);
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }
    private void goEndFragment() {
        Navigation.findNavController(getView()).navigate(R.id.endFragment);

    }

    private void displayImage(Question question, Context appContext) {
        String fileName = ImagesDownloader.scapeName(question.getPossibleCities().get(0).getName());
        int resId = appContext.getResources().getIdentifier(fileName, "drawable", appContext.getPackageName());
        Swaperphoto.setImageResource(resId);
    }

    private void displayTextButton(Question question) {
        button20.setText(question.getPossibleCities().get(0).getName());
        button21.setText(question.getPossibleCities().get(1).getName());
        button22.setText(question.getPossibleCities().get(2).getName());
        button23.setText(question.getPossibleCities().get(3).getName());
        button24.setText(question.getPossibleCities().get(4).getName());
        button25.setText(question.getPossibleCities().get(5).getName());

    }

    private void displayGame(Question question) {
        displayImage(question, Swaperphoto.getContext());
        displayTextButton(question);

    }





    @OnClick({R.id.button20, R.id.button21, R.id.button22, R.id.button23, R.id.button24, R.id.button25})
    public void onViewClicked(View view) {
        Game game =mViewModel.getGame();
        switch (view.getId()) {
            case R.id.button20:
                answerQuestion(game,0);
                break;
            case R.id.button21:
                answerQuestion(game,1);
                break;
            case R.id.button22:
                answerQuestion(game,2);
                break;
            case R.id.button23:
                answerQuestion(game,3);
                break;
            case R.id.button24:
                answerQuestion(game,4);
                break;
            case R.id.button25:
                answerQuestion(game,5);
                break;
        }
    }
    private void answerQuestion(Game game, int position) {
        Question question=game.getCurrentQuestion();

        if (game.isFinished()) {
            goEndFragment();
        } else {
            mViewModel.answerQuestion(position);
            game.getScore();
            displayGame(question);
        }
    }
}
