package cat.itb.cityquiz.Presentation.screens;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;

public class EndFragment extends Fragment {


    @BindView(R.id.ScoreNumber)
    TextView ScoreNumber;
    @BindView(R.id.RestarGAme)
    Button RestarGAme;
    private ChooseViewModel ChooseViewModel;

    public static EndFragment newInstance() {
        return new EndFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.end_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ChooseViewModel = ViewModelProviders.of(getActivity()).get(ChooseViewModel.class);
        // TODO: Use the ViewModel
        Game game = ChooseViewModel.getGame();
        setScore(game);

    }

    private void setScore(Game game) {
        String score = Integer.valueOf(game.getNumCorrectAnswers()).toString();
        ScoreNumber.setText(score);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }


    @OnClick(R.id.RestarGAme)
    public void onViewClicked() {
        ChooseViewModel.startGame();
        Navigation.findNavController(getView()).navigate(R.id.chooseFragment);

    }
}
