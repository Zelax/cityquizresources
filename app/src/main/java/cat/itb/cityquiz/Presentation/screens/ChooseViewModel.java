package cat.itb.cityquiz.Presentation.screens;

import androidx.lifecycle.ViewModel;

import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class ChooseViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    Game game;
    GameLogic gameLogic= RepositoriesFactory.getGameLogic();


    public void startGame(){
       game=gameLogic.createGame(Game.maxQuestions,Game.possibleAnswers);

    }

    public void answerQuestion(int position) {
    game=gameLogic.answerQuestions(getGame(),position);

    }

    public Game getGame() {
        return game;
    }

    /*public void result(){
        game.isFinished();
        game.getNumCorrectAnswers();
        game.getScore();

    }*/

}
